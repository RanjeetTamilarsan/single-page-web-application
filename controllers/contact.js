'use strict';
angular.module('demoApp').controller('contactController', ['$scope', '$location',
    function ($scope, $location) {

        /* google maps */
        var cities = [
            {
                city: 'CA',
                desc: 'USA Office',
                lat: 37.37601,
                long: -122.04974
            },
            {
                city: 'Comibatore',
                desc: 'India office in coimbatore',
                lat: 11.03172,
                long: 77.01866
            }
        ];

        var mapOptions = {
            zoom: 2,
            center: new google.maps.LatLng(40.46367, -3.74922),
            mapTypeId: google.maps.MapTypeId.TERRAIN
        }

        $scope.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        $scope.markers = [];

        var infoWindow = new google.maps.InfoWindow();

        var createMarker = function (info) {

            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng(info.lat, info.long),
                title: info.city
            });
            marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });

            $scope.markers.push(marker);

        }

        for (var i = 0; i < cities.length; i++) {
            createMarker(cities[i]);
        }

        $scope.openInfoWindow = function (e, selectedMarker) {
            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
        }

    }]);
