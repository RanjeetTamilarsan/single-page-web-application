// create the module and name it demo app
var demoApp = angular.module('demoApp', ['ngRoute']);

// configure our routes
demoApp.config(function($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl : 'views/home.html',
            controller  : 'mainController'
        })

        .when('/home', {
            templateUrl : 'views/home.html',
            controller  : 'mainController'
        })

        .when('/about', {
            templateUrl : 'views/about-us.html',
            controller  : 'aboutController'
        })

        .when('/product', {
            templateUrl : 'views/product.html',
            controller  : 'productController'
        })

        .when('/features', {
            templateUrl : 'views/features.html',
            controller  : 'featuresController'
        })

        .when('/plan', {
            templateUrl : 'views/plan.html',
            controller  : 'planController'
        })

        .when('/contact', {
            templateUrl : 'views/contact-us.html',
            controller  : 'contactController'
        });
});